# neo-Crampon-libre

Ce dépôt contient le projet de la néo-Crampon Libre, une modernisation de la traduction catholique française de Crampon sous licence CC. Il propose de contribuer à une bible de qualité, avec références croisées, notes et introductions.
Version 0.4

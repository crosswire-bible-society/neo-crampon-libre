Version 0.3
Le 04-04-2023: version 0.4
Le 23-09-2023: version 0.5 Correction de 1Tim 5,17-18. Ajout complet des références AT dans Marc avec balises \qt. Correction des pertes de certaines majuscules durant l'ajout des nombres de strong.
##Titres
Titres dans Genèse, Exode et Marc, 2Jn, 3Jn.
##Références croisées
Références croisées dans Marc
Références croisées dans Jean
Références croisées dans Matthieu
Références croisées dans 3Jn
##Strong
Mat 1,1 vérifié
Marc 1 à 2,14 vérifié
Marc 16 vérifié
2Jn complètement vérifié
3Jn complètement vérifié
1Tim 5,17-18 vérifié
Problème:

1,24 début du verset.
Désormais toutes la bible est en strong grâce à Michael. Mais il faut tout relire, sauf Mc 1 à 2,14 et chap 16.
## Ajout des citations AT dans le NT
 Ajout complet des références AT dans Marc avec balises \qt
##Tobit
La traduction de Tobie dans l'original de Crampon est basée sur la Vulgate. Celle que nous proposons est une traduction de fusion. C'est à dire que les meilleurs traductions françaises italiennes et anglaises sont mises en parallèles, et tout le texte commun est fusionné.
Relecture complète.

##Psaumes
Ajout des balises pour sélah et l'alphabet hébreux, et des titres des psaumes bien que cela ne soit pas actif dans xiphos.

##AT
Contient les nombres des strongs grâce à l'IA de Michael

##Autres
Vouvoument corrigé dans les Evangiles.
